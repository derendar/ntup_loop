#define Btag_cxx
#include "Btag.h"




//// compile me like that:
//// g++ -Wall -std=c++1y -I `root-config --incdir` -o loop_jz Btag.cpp  `root-config --libs`
//// or with roounflod
//// g++ -Wall -std=c++1y /people/plgdomienik/RooUnfold/libRooUnfold.so -I/people/plgdomienik/RooUnfold/src -I `root-config --incdir` -o loop_jz Btag.cpp  `root-config --libs`


TChain* ChainBuilder(const char* chainName, const char* fileName);


int main (int argc, char *argv[])
{

//// ******** arguments: 
/// 1 - list of files (ntuples) in text file  
/// 2 - bool select b jets ("1" means yes, anything else no)
/// 3 - bool select c jets ("1" means yes, anything else no) 
/// 4 - bool select u jets ("1" means yes, anything else no)
///	5 - int do taggin based on DL1 ("0" no; "1" tagging; "-1" anti tagging)
/// 6 - bool do Et reweighting ("1" means yes) 
/// 7 - bool do Pt reweighting ("1" means yes) 

	cout << "Number of arguments " << argc << endl;
	if(argc ==  7) 
	{
		cout << "File list =  " << argv[1] << endl;
		cout << "Select b jets =  " << argv[2] << endl;
		cout << "Select c jets =  " << argv[3] << endl;
		cout << "Select u jets =  " << argv[4] << endl;
		cout << "Do Et reweighting = " << argv[5] << endl;
		cout << "Do Pt reweighting = " << argv[6] << endl;
 
		//TChain* mychain = ChainBuilder("bTag_AntiKt4HIJets", argv[1]);
		//TChain* mychain = ChainBuilder("bTag_DFAntiKt4HIJets", argv[1]);
		
		TString out_file_name = "output_histograms";
		if(strcmp(argv[2], "1") == 0) out_file_name = out_file_name + "_b_jets"; 
		if(strcmp(argv[3], "1") == 0) out_file_name = out_file_name + "_c_jets";  
		if(strcmp(argv[4], "1") == 0) out_file_name = out_file_name + "_u_jets";
		// if(strcmp(argv[5], "1") == 0) out_file_name = out_file_name + "_DL1tagged";
		// if(strcmp(argv[5], "-1") == 0) out_file_name = out_file_name + "_DL1untagged";
		if(strcmp(argv[5], "1") == 0) out_file_name = out_file_name + "_Et_r";
		if(strcmp(argv[6], "1") == 0) out_file_name = out_file_name + "_Pt_r";         
		out_file_name = out_file_name + ".root"; 

                TString tree_name = ""; 
                if ( (strcmp(argv[2], "0") == 0) && 
                     (strcmp(argv[3], "0") == 0) && 
                     (strcmp(argv[4], "0") == 0) ) 
			tree_name = "bTag_DFAntiKt4HIJets"; 
		else 
			tree_name = "bTag_AntiKt4HIJets";
 
    const char* chain_tree_name = tree_name; 
    std::cout << "Name of tree " << chain_tree_name << std::endl; 
		TChain* mychain = ChainBuilder(chain_tree_name, argv[1]);

		TFile *outfile = new TFile(out_file_name,"RECREATE");
		Btag t((TTree*)mychain, outfile, argv[2], argv[3], argv[4], argv[5], argv[6]);
		t.Loop();
	}
	else 
		cout << "7 arguments expected!  " <<  argc << " provided" << endl;

	return 0;
}


TChain* ChainBuilder(const char* chainName, const char* fileName)
{
  TChain* my_chain = new TChain(chainName);

  string filePath;
  ifstream input(fileName);

  if(input.is_open())
  {
    while(!input.eof())
    {
      getline(input,filePath);
      cout << filePath << endl;
      my_chain->Add(filePath.c_str());
    }
  }
  my_chain->GetEntries(); //dummy
  return my_chain;
}


void Btag::Loop()
{
//   In a ROOT session, you can do:
//      root> .L Btag.C
//      root> Btag t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//
//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch

	if (fChain == 0) return;

	Long64_t nentries = fChain->GetEntriesFast();
	cout << "Entries in the chain " << nentries << endl;

	JetsBtagInputs jets_hists_all("jets_all_cent_all_GeV");
	JetsBtagInputs jets_hists_70_80_all_GeV("jets_70_80_all_GeV");
	JetsBtagInputs jets_hists_70_80_50_100_GeV("jets_70_80_50_100_GeV");
	JetsBtagInputs jets_hists_70_80_100_200_GeV("jets_70_80_100_200_GeV");
	JetsBtagInputs jets_hists_70_80_200_300_GeV("jets_70_80_200_300_GeV");
	JetsBtagInputs jets_hists_70_80_300_500_GeV("jets_70_80_300_500_GeV");
	JetsBtagInputs jets_hists_70_80_500_1000_GeV("jets_70_80_500_1000_GeV");

	JetsBtagInputs jets_hists_30_40_all_GeV("jets_30_40_all_GeV");
	JetsBtagInputs jets_hists_30_40_50_100_GeV("jets_30_40_50_100_GeV");
	JetsBtagInputs jets_hists_30_40_100_200_GeV("jets_30_40_100_200_GeV");
	JetsBtagInputs jets_hists_30_40_200_300_GeV("jets_30_40_200_300_GeV");
	JetsBtagInputs jets_hists_30_40_300_500_GeV("jets_30_40_300_500_GeV");
	JetsBtagInputs jets_hists_30_40_500_1000_GeV("jets_30_40_500_1000_GeV");

	JetsBtagInputs jets_hists_0_10_all_GeV("jets_0_10_all_GeV");
	JetsBtagInputs jets_hists_0_10_50_100_GeV("jets_0_10_50_100_GeV");
	JetsBtagInputs jets_hists_0_10_100_200_GeV("jets_0_10_100_200_GeV");
	JetsBtagInputs jets_hists_0_10_200_300_GeV("jets_0_10_200_300_GeV");
	JetsBtagInputs jets_hists_0_10_300_500_GeV("jets_0_10_300_500_GeV");
	JetsBtagInputs jets_hists_0_10_500_1000_GeV("jets_0_10_500_1000_GeV");

	Long64_t nbytes = 0, nb = 0;

	//// restrict number of events
	//	nentries = 1000;
	//// LOOP OVER EVENTS
	for (Long64_t jentry=0; jentry<nentries;jentry++)
	{
		Long64_t ientry = LoadTree(jentry);
		if (ientry < 0) break;
		nb = fChain->GetEntry(jentry);   nbytes += nb;
		if (ientry%1000 == 0) cout << "Event " << ientry << endl; 
		double Et_weight = 1.0;
		if( m_do_Et_weight ) Et_weight = getWeight(FCalEt, m_h_et_weight);
		m_h_FCal_all->Fill(FCalEt, Et_weight);

		m_jet_selected_FCalEt = -1000.0;  

		bool pass_j50 = false; 
		bool pass_j60 = false;
		bool pass_j85 = false; 
		bool pass_j100 = false; 
		//bool pass_j50_te20 = false; 
		bool pass_j60_te50 = false; 
		bool pass_j75_te50 = false; 

		if( !m_isMC)
		{
			pass_j50 = trigg_j50; 
			pass_j60 = trigg_j60;
			pass_j85 = trigg_j85; 
			pass_j100 = trigg_j100; 
			//pass_j50_te20 = trigg_j50_te20; 
			pass_j60_te50 = trigg_j60_te50; 
			pass_j75_te50 = trigg_j75_te50; 
		}

		bool event_pass_trigg = pass_j50 || pass_j60 || pass_j85 || pass_j100 || pass_j60_te50 || pass_j75_te50 ; 
		if( !m_isMC)	
			if(!event_pass_trigg )  continue; 


		m_h_FCal->Fill(FCalEt, Et_weight);

		int num_of_jets = jet_pt->size();

		int couter_all = 0; 
		int couter_70_80_all_GeV = 0;
		int couter_70_80_50_100_GeV = 0;
		int couter_70_80_100_200_GeV = 0;
		int couter_70_80_200_300_GeV = 0;
		int couter_70_80_300_500_GeV = 0;
		int couter_70_80_500_1000_GeV = 0;
		int couter_30_40_all_GeV = 0;
		int couter_30_40_50_100_GeV = 0;
		int couter_30_40_100_200_GeV = 0;
		int couter_30_40_200_300_GeV = 0;
		int couter_30_40_300_500_GeV = 0;
		int couter_30_40_500_1000_GeV = 0;
		int couter_0_10_all_GeV = 0;
		int couter_0_10_50_100_GeV = 0;
		int couter_0_10_100_200_GeV = 0;
		int couter_0_10_200_300_GeV = 0;
		int couter_0_10_300_500_GeV = 0;
		int couter_0_10_500_1000_GeV = 0;



		//// LOOP over jets
		for (int i=0; i<num_of_jets; i++)
		{
		/// if selecting leadig jet 
		//num_of_jets = 1; 
			
			float jetDL1 = 0.0; 
			float fc = 0.07;
			if (jet_dl1_pc->at(i) >0 && jet_dl1_pu->at(i) > 0)
				jetDL1 = log( (jet_dl1_pb->at(i)) / (fc*jet_dl1_pc->at(i) + (1-fc)*jet_dl1_pu->at(i)) );
			else
				jetDL1 = 0;
          
  
			if(m_doDL1_tagging == 1 )  /// tagging 
			{
				if(FCalEt > m_fcal80 && FCalEt < m_fcal70) if(jetDL1 < 1.91002) continue;
				if(FCalEt > m_fcal40 && FCalEt < m_fcal30) if(jetDL1 < 1.59893) continue; 
				if(FCalEt > m_fcal10)                      if(jetDL1 < 1.01511) continue;
			}
			if(m_doDL1_tagging == -1 ) /// anti-tagging
			{
				if(FCalEt > m_fcal80 && FCalEt < m_fcal70) if(jetDL1 > 1.91002) continue;
				if(FCalEt > m_fcal40 && FCalEt < m_fcal30) if(jetDL1 > 1.59893) continue; 
				if(FCalEt > m_fcal10)                      if(jetDL1 > 1.01511) continue;
			}

			//if(jetDL1 < 2.00) continue; 
			//if(jetDL1 < 1.75) continue; 
		
			if( m_isMC )
			{ 
				if( m_do_sel_b )
				{ 
					if (jet_LabDr_HadF->at(i) != 5) continue;
				}

				if( m_do_sel_c )
				{
					if (jet_LabDr_HadF->at(i) != 4) continue;
				}

			
				if( m_do_sel_u )  
				{
					if (jet_LabDr_HadF->at(i) != 0) continue; 
					if (jet_truthMatch->at(i) == 0) continue;

			        //cout << "event FCal Et: " << FCalEt << endl; 
			        //cout << "\tjet_LabDr_HadF: " << jet_LabDr_HadF->at(i) << endl; 
			        //cout << "\tjet_truthMatch:  " << jet_truthMatch->at(i) << endl;  
				}
			}

			/// temporary kinematic cuts
			//if( jet_pt->at(i)*0.001 < 50 || 
			//    jet_pt->at(i)*0.001 > 500 || 
			//    abs(jet_eta->at(i))>2.1) continue;  

			if ( abs(jet_eta->at(i))>2.1 ) continue;

			bool pass_j50_pt = false;  
			bool pass_j60_pt = false;  
			bool pass_j85_pt = false;
			bool pass_j100_pt = false; 
			//bool pass_j50_te20_pt = false; 
			bool pass_j60_te50_pt = false; 
			bool pass_j75_te50_pt = false; 
			if ( !m_isMC )
			{
				pass_j50_pt = (trigg_j50 == true && jet_pt->at(i)*0.001 > 79. && jet_pt->at(i)*0.001 < 84.); 
				pass_j60_pt = (trigg_j60 == true && jet_pt->at(i)*0.001 > 84. && jet_pt->at(i)*0.001 < 112.);
				pass_j85_pt = (trigg_j85 == true && jet_pt->at(i)*0.001 > 112. && jet_pt->at(i)*0.001 < 124.); 
				pass_j100_pt = (trigg_j100 == true && jet_pt->at(i)*0.001 > 124.);

				/// 2015 selection
				//pass_j50_te20_pt = (trigg_j50_te20 == true && jet_pt->at(i)*0.001 > 68.1 && jet_pt->at(i)*0.001 < 79.4);
				pass_j60_te50_pt = (trigg_j60_te50 == true && jet_pt->at(i)*0.001 > 79.4 && jet_pt->at(i)*0.001 < 89.1);
				pass_j75_te50_pt = (trigg_j75_te50 == true && jet_pt->at(i)*0.001 > 89.1 ); 

			}
			bool event_pass_trigg_pt = pass_j50_pt || pass_j60_pt || pass_j85_pt || pass_j100_pt || pass_j60_te50_pt || pass_j75_te50_pt; 

			if ( !m_isMC ) /// in other words if data 
				if(!event_pass_trigg_pt )  continue; 

			double jet_pt_weight = 1.0;
			if( m_do_Et_weight ) jet_pt_weight = Et_weight;
			//if( m_do_Pt_weight ) jet_pt_weight = Et_weight * getWeight(jet_pt->at(i)*0.001, m_h_pt_weight); 
			if( m_do_Pt_weight ) jet_pt_weight = Et_weight * getWeight2D(jet_eta->at(i), jet_pt->at(i)*0.001, m_h_etapt_weight); 

			//if (pass_j50_pt)  jet_pt_weight = 1./98.6; 
			//if (pass_j60_pt)  jet_pt_weight = 1./274.6; 
			//if (pass_j85_pt)  jet_pt_weight = 1./1720.; 
			//if (pass_j100_pt) jet_pt_weight = 1./1720.; 
			if (pass_j50_pt)  jet_pt_weight = 1720./98.6; 
			if (pass_j60_pt)  jet_pt_weight = 1720./274.6; 
			if (pass_j85_pt)  jet_pt_weight = 1.; 
			if (pass_j100_pt) jet_pt_weight = 1.; 
			//if (pass_j50_pt)  jet_pt_weight = 1.; 
			//if (pass_j60_pt)  jet_pt_weight = 1.; 
			//if (pass_j85_pt)  jet_pt_weight = 1.; 
			//if (pass_j100_pt) jet_pt_weight = 1.; 
			
                        //if (pass_j50_te20_pt) jet_pt_weight = 497.7/25.8; 
			if (pass_j60_te50_pt) jet_pt_weight = 497.7/69.9; 
			if (pass_j75_te50_pt) jet_pt_weight = 1. ; 

			fill_jets_histograms(jets_hists_all, i, jet_pt_weight);
			couter_all++; 

			if( m_do_Pt_weight )
			{
				if(FCalEt > m_fcal80 && FCalEt < m_fcal70) jet_pt_weight = Et_weight * getWeight2D(jet_eta->at(i), jet_pt->at(i)*0.001, m_h_etapt_weight_70_80);
				if(FCalEt > m_fcal40 && FCalEt < m_fcal30) jet_pt_weight = Et_weight * getWeight2D(jet_eta->at(i), jet_pt->at(i)*0.001, m_h_etapt_weight_30_40);
				if(FCalEt > m_fcal10) 			   jet_pt_weight = Et_weight * getWeight2D(jet_eta->at(i), jet_pt->at(i)*0.001, m_h_etapt_weight_0_10);
			}

			if(FCalEt > m_fcal80 && FCalEt < m_fcal70) {couter_70_80_all_GeV++; fill_jets_histograms(jets_hists_70_80_all_GeV, i, jet_pt_weight);}
			if(FCalEt > m_fcal40 && FCalEt < m_fcal30) {couter_30_40_all_GeV++; fill_jets_histograms(jets_hists_30_40_all_GeV, i, jet_pt_weight);}
			if(FCalEt > m_fcal10) {couter_0_10_all_GeV++; fill_jets_histograms(jets_hists_0_10_all_GeV, i, jet_pt_weight);}

			if(jet_pt->at(i) > 50000 && jet_pt->at(i) < 100000)
			{
				if(FCalEt > m_fcal80 && FCalEt < m_fcal70) {couter_70_80_50_100_GeV++; fill_jets_histograms(jets_hists_70_80_50_100_GeV, i, jet_pt_weight);}
				if(FCalEt > m_fcal40 && FCalEt < m_fcal30) {couter_30_40_50_100_GeV++; fill_jets_histograms(jets_hists_30_40_50_100_GeV, i, jet_pt_weight);}
				if(FCalEt > m_fcal10) {couter_0_10_50_100_GeV++; fill_jets_histograms(jets_hists_0_10_50_100_GeV, i, jet_pt_weight);}

			}
			if(jet_pt->at(i) > 100000 && jet_pt->at(i) < 200000)
			{
				if(FCalEt > m_fcal80 && FCalEt < m_fcal70) {couter_70_80_100_200_GeV++; fill_jets_histograms(jets_hists_70_80_100_200_GeV, i, jet_pt_weight);}
				if(FCalEt > m_fcal40 && FCalEt < m_fcal30) {couter_30_40_100_200_GeV++; fill_jets_histograms(jets_hists_30_40_100_200_GeV, i, jet_pt_weight);}
				if(FCalEt > m_fcal10) {couter_0_10_100_200_GeV++; fill_jets_histograms(jets_hists_0_10_100_200_GeV, i, jet_pt_weight);}
			}
			if(jet_pt->at(i) > 200000 && jet_pt->at(i) < 300000)
			{
				if(FCalEt > m_fcal80 && FCalEt < m_fcal70) {couter_70_80_200_300_GeV++; fill_jets_histograms(jets_hists_70_80_200_300_GeV, i, jet_pt_weight);}
				if(FCalEt > m_fcal40 && FCalEt < m_fcal30) {couter_30_40_200_300_GeV++; fill_jets_histograms(jets_hists_30_40_200_300_GeV, i, jet_pt_weight);}
				if(FCalEt > m_fcal10) {couter_0_10_200_300_GeV++; fill_jets_histograms(jets_hists_0_10_200_300_GeV, i, jet_pt_weight);}
			}
			if(jet_pt->at(i) > 300000 && jet_pt->at(i) < 500000)
			{
				if(FCalEt > m_fcal80 && FCalEt < m_fcal70) {couter_70_80_300_500_GeV++; fill_jets_histograms(jets_hists_70_80_300_500_GeV, i, jet_pt_weight);}
				if(FCalEt > m_fcal40 && FCalEt < m_fcal30) {couter_30_40_300_500_GeV++; fill_jets_histograms(jets_hists_30_40_300_500_GeV, i, jet_pt_weight);}
				if(FCalEt > m_fcal10) {couter_0_10_300_500_GeV++; fill_jets_histograms(jets_hists_0_10_300_500_GeV, i, jet_pt_weight);}
			}
			if(jet_pt->at(i) > 500000 && jet_pt->at(i) < 1000000)
			{
				if(FCalEt > m_fcal80 && FCalEt < m_fcal70) {couter_70_80_500_1000_GeV++; fill_jets_histograms(jets_hists_70_80_500_1000_GeV, i, jet_pt_weight);}
				if(FCalEt > m_fcal40 && FCalEt < m_fcal30) {couter_30_40_500_1000_GeV++; fill_jets_histograms(jets_hists_30_40_500_1000_GeV, i, jet_pt_weight);}
				if(FCalEt > m_fcal10) {couter_0_10_500_1000_GeV++; fill_jets_histograms(jets_hists_0_10_500_1000_GeV, i, jet_pt_weight);}
			}

			if(FCalEt > m_fcal10)			
			{
				m_h_jetPt_cent->Fill (jet_pt->at(i) * 0.001); 
				if(jet_truthPt->at(i) > 0) 
				{
					m_h_jetTruthPt_cent -> Fill (jet_truthPt->at(i) * 0.001); 
					m_response_cent->Fill(jet_pt->at(i) * 0.001, jet_truthPt->at(i) * 0.001);
				} 
			}	 
		}

		jets_hists_all.m_h_numOfJets->Fill(couter_all);
		jets_hists_70_80_all_GeV.m_h_numOfJets->Fill(couter_70_80_all_GeV);
		jets_hists_70_80_50_100_GeV.m_h_numOfJets->Fill(couter_70_80_50_100_GeV);
		jets_hists_70_80_100_200_GeV.m_h_numOfJets->Fill(couter_70_80_100_200_GeV);
		jets_hists_70_80_200_300_GeV.m_h_numOfJets->Fill(couter_70_80_200_300_GeV);
		jets_hists_70_80_300_500_GeV.m_h_numOfJets->Fill(couter_70_80_300_500_GeV);
		jets_hists_70_80_500_1000_GeV.m_h_numOfJets->Fill(couter_70_80_500_1000_GeV);

		jets_hists_30_40_all_GeV.m_h_numOfJets->Fill(couter_30_40_all_GeV);
		jets_hists_30_40_50_100_GeV.m_h_numOfJets->Fill(couter_30_40_50_100_GeV);
		jets_hists_30_40_100_200_GeV.m_h_numOfJets->Fill(couter_30_40_100_200_GeV);
		jets_hists_30_40_200_300_GeV.m_h_numOfJets->Fill(couter_30_40_200_300_GeV);
		jets_hists_30_40_300_500_GeV.m_h_numOfJets->Fill(couter_30_40_300_500_GeV);
		jets_hists_30_40_300_500_GeV.m_h_numOfJets->Fill(couter_30_40_500_1000_GeV);

		jets_hists_0_10_all_GeV.m_h_numOfJets->Fill(couter_0_10_all_GeV);
		jets_hists_0_10_50_100_GeV.m_h_numOfJets->Fill(couter_0_10_50_100_GeV);
		jets_hists_0_10_100_200_GeV.m_h_numOfJets->Fill(couter_0_10_100_200_GeV);
		jets_hists_0_10_200_300_GeV.m_h_numOfJets->Fill(couter_0_10_200_300_GeV);
		jets_hists_0_10_300_500_GeV.m_h_numOfJets->Fill(couter_0_10_300_500_GeV);
		jets_hists_0_10_300_500_GeV.m_h_numOfJets->Fill(couter_0_10_500_1000_GeV);


		// if (Cut(ientry) < 0) continue;
	} //// END OF LOOP OVER EVENTS
	cout << "Exit from the main loop " << endl;

  
  RooUnfoldBayes unfold (m_response_cent, m_h_jetPt_cent, 4);
  m_h_jetPt_cent_unfolded = (TH1D*) unfold.Hreco();


	write_histograms(jets_hists_all);


	write_histograms(jets_hists_70_80_all_GeV);
	write_histograms(jets_hists_70_80_50_100_GeV);
	write_histograms(jets_hists_70_80_100_200_GeV);
	write_histograms(jets_hists_70_80_200_300_GeV);
	write_histograms(jets_hists_70_80_300_500_GeV);
	write_histograms(jets_hists_70_80_500_1000_GeV);

	write_histograms(jets_hists_30_40_all_GeV);
	write_histograms(jets_hists_30_40_50_100_GeV);
	write_histograms(jets_hists_30_40_100_200_GeV);
	write_histograms(jets_hists_30_40_200_300_GeV);
	write_histograms(jets_hists_30_40_300_500_GeV);
	write_histograms(jets_hists_30_40_500_1000_GeV);

	write_histograms(jets_hists_0_10_all_GeV);
	write_histograms(jets_hists_0_10_50_100_GeV);
	write_histograms(jets_hists_0_10_100_200_GeV);
	write_histograms(jets_hists_0_10_200_300_GeV);
	write_histograms(jets_hists_0_10_300_500_GeV);
	write_histograms(jets_hists_0_10_500_1000_GeV);

  
	m_outHistFile->cd();
	m_h_FCal->Write();
	m_h_FCal_all->Write();
	m_response_cent->Write(); 
	m_h_jetPt_cent->Write();
	m_h_jetPt_cent_unfolded->Write(); 
	m_h_jetTruthPt_cent->Write();

	m_outHistFile->Close(); 


}

void Btag::fill_jets_histograms(JetsBtagInputs &hist_collection, int pos, double weight)
{
	//	cout << "\tjet pT = " <<jet_pt->at(pos) << "MeV, pT = " << jet_pt->at(pos)*0.001 << "GeV"  <<endl;
	hist_collection.m_h_jetPt->Fill((jet_pt->at(pos)) * 0.001, weight);
	hist_collection.m_h_jetEta->Fill(jet_eta->at(pos), weight);
  hist_collection.m_h_jetEtaPt->Fill(jet_eta->at(pos), jet_pt->at(pos) * 0.001, weight); 
	hist_collection.m_h_jetEtaPhi->Fill(jet_eta->at(pos), jet_phi->at(pos), weight); 


	if(m_jet_selected_FCalEt < -100.0)
	{
		m_jet_selected_FCalEt = FCalEt;
		double Et_weight = 1.0; 
		if( m_do_Et_weight ) Et_weight = getWeight(FCalEt, m_h_et_weight);
		hist_collection.m_h_FCalEt_events_with_jet->Fill(m_jet_selected_FCalEt, Et_weight);  
	}

	//// #### JetFitter
	if(jet_jf_m->at(pos) > -100 && jet_jf_m->at(pos) < 12000)
	{ 
		hist_collection.m_h_jetJetFitter_mass->Fill(jet_jf_m->at(pos), weight); 
	}
	else 
	{
		hist_collection.m_h_jetJetFitter_mass->Fill(0.0, weight);
	}

	if(jet_jf_efc->at(pos) > -0.01 && jet_jf_efc->at(pos) < 1.21) 
		hist_collection.m_h_jetJetFitter_energy_fraction->Fill(jet_jf_efc->at(pos), weight);
	else 
		hist_collection.m_h_jetJetFitter_energy_fraction->Fill(0.0, weight);

	if(jet_jf_sig3d->at(pos) > -1.0 && jet_jf_sig3d->at(pos) < 100) 
		hist_collection.m_h_jetJetFitter_significance3d->Fill(jet_jf_sig3d->at(pos), weight);
	else 
		hist_collection.m_h_jetJetFitter_significance3d->Fill(0.0, weight);

	if(jet_jf_nvtx->at(pos) > 0.0 && jet_jf_nvtx->at(pos) < 10) 
		hist_collection.m_h_jetJetFitter_nVTX->Fill(jet_jf_nvtx->at(pos), weight);
	else 
		hist_collection.m_h_jetJetFitter_nVTX->Fill(0.0, weight);

	if(jet_jf_nvtx1t->at(pos) > 0.0)
		hist_collection.m_h_jetJetFitter_nSingleTracks->Fill(jet_jf_nvtx1t->at(pos), weight);
	else 
		hist_collection.m_h_jetJetFitter_nSingleTracks->Fill(0.0, weight);

	if(jet_jf_ntrkAtVx->at(pos) > 0.0)
		hist_collection.m_h_jetJetFitter_nTracksAtVtx->Fill(jet_jf_ntrkAtVx->at(pos), weight);
	else 
		hist_collection.m_h_jetJetFitter_nTracksAtVtx->Fill(0.0, weight);

	if(jet_jf_n2t->at(pos) > 0.0) 
		hist_collection.m_h_jetJetFitter_N2Tpair->Fill(jet_jf_n2t->at(pos), weight);
	else 
		hist_collection.m_h_jetJetFitter_N2Tpair->Fill(0.0, weight);

	hist_collection.m_h_jetJetFitter_deltaR->Fill(jet_jf_dR->at(pos), weight);

	//// #### IP2/3D
	if (jet_ip2d_pu->at(pos) != 0.0 && jet_ip2d_pb->at(pos) != 0.0)
		hist_collection.m_h_jetIP2D_bu->Fill(log(jet_ip2d_pb->at(pos)/jet_ip2d_pu->at(pos)), weight);
	else
		hist_collection.m_h_jetIP2D_bu->Fill(-21.0, weight);

	if (jet_ip2d_pc->at(pos) != 0.0 && jet_ip2d_pb->at(pos) != 0.0)
		hist_collection.m_h_jetIP2D_bc->Fill(log(jet_ip2d_pb->at(pos)/jet_ip2d_pc->at(pos)), weight);
	else
		hist_collection.m_h_jetIP2D_bc->Fill(-21.0, weight);

	if (jet_ip2d_pu->at(pos) != 0.0 && jet_ip2d_pc->at(pos) != 0.0)
		hist_collection.m_h_jetIP2D_cu->Fill(log(jet_ip2d_pc->at(pos)/jet_ip2d_pu->at(pos)), weight);
	else
		hist_collection.m_h_jetIP2D_cu->Fill(0.0, weight);

	if (jet_ip3d_pu->at(pos) != 0.0 && jet_ip3d_pb->at(pos) != 0.0)
		hist_collection.m_h_jetIP3D_bu->Fill(log(jet_ip3d_pb->at(pos)/jet_ip3d_pu->at(pos)), weight);
	else
		hist_collection.m_h_jetIP3D_bu->Fill(-21.0, weight);

	if (jet_ip3d_pc->at(pos) != 0.0 && jet_ip3d_pb->at(pos) != 0.0)
		hist_collection.m_h_jetIP3D_bc->Fill(log(jet_ip3d_pb->at(pos)/jet_ip3d_pc->at(pos)), weight);
	else
		hist_collection.m_h_jetIP3D_bc->Fill(-21.0, weight);

	if (jet_ip3d_pu->at(pos) != 0.0 && jet_ip3d_pc->at(pos) != 0.0)
		hist_collection.m_h_jetIP3D_cu->Fill(log(jet_ip3d_pc->at(pos)/jet_ip3d_pu->at(pos)), weight);
	else
		hist_collection.m_h_jetIP3D_cu->Fill(0.0, weight);

	//// #### DL1 
	double dl1_disciminant = 0; 
	double dl1_disciminant_two_flav = 0; 
	double c_fraction = 0.07;
	double denominator = c_fraction*jet_dl1_pc->at(pos) + (1-c_fraction)*jet_dl1_pu->at(pos); 

	if (denominator > 0.0 || denominator < 0.0) 
		dl1_disciminant = log(jet_dl1_pb->at(pos)/denominator); 
	else 
		dl1_disciminant = -39.0; 

	if(jet_dl1_pu->at(pos) > 0.0 || jet_dl1_pu->at(pos) < 0.0)
		dl1_disciminant_two_flav = log(jet_dl1_pb->at(pos)/jet_dl1_pu->at(pos));
	else 
		dl1_disciminant_two_flav = -39.0;  

	hist_collection.m_h_jetDL1->Fill(dl1_disciminant, weight);
	hist_collection.m_h_jetDL1_two_flav->Fill(dl1_disciminant_two_flav, weight);
	hist_collection.m_h_jetDL1_pb->Fill(jet_dl1_pb->at(pos), weight);
	hist_collection.m_h_jetDL1_pc->Fill(jet_dl1_pc->at(pos), weight);
	hist_collection.m_h_jetDL1_pu->Fill(jet_dl1_pu->at(pos), weight);

	//// #### SV1

	hist_collection.m_h_jetSV1_NGTinSvx->Fill(jet_sv1_ntrkv->at(pos), weight);

	if(jet_sv1_m->at(pos) > -100.0 && jet_sv1_m->at(pos) < 10000.0)
		hist_collection.m_h_jetSV1_mass->Fill(jet_sv1_m->at(pos), weight);
	else 
		hist_collection.m_h_jetSV1_mass->Fill(-100.0, weight);

	hist_collection.m_h_jetSV1_N2Tpair->Fill(jet_sv1_n2t->at(pos), weight);
	hist_collection.m_h_jetSV1_efracsvx->Fill(jet_sv1_efc->at(pos), weight);

	if(jet_sv1_deltaR->at(pos) > 0.0)
		hist_collection.m_h_jetSV1_deltaR->Fill(jet_sv1_deltaR->at(pos), weight);
	else 
		hist_collection.m_h_jetSV1_deltaR->Fill(0.0, weight);

	hist_collection.m_h_jetSV1_Lxy->Fill(jet_sv1_Lxy->at(pos), weight);
	hist_collection.m_h_jetSV1_L3d->Fill(jet_sv1_L3d->at(pos), weight);
	hist_collection.m_h_jetSV1_normdist->Fill(jet_sv1_normdist->at(pos), weight);

	if(jet_muon_dR->at(pos) > 0 && jet_muon_dR->at(pos) < 0.3 && jet_muon_pT->at(pos) < 500000)
	{
		hist_collection.m_h_jet_muon_dR->Fill (jet_muon_dR->at(pos)); 
		hist_collection.m_h_jet_muon_pT->Fill (jet_muon_pT->at(pos)* 0.001); 
		hist_collection.m_h_jet_muon_pTRel->Fill (jet_muon_pTRel->at(pos));
	}

}

void Btag::write_histograms(JetsBtagInputs &hist_collection)
{
	gFile = m_outHistFile;
	m_outHistFile->cd();
	TDirectory *subdir = m_outHistFile->mkdir(hist_collection.m_subDir_name);
	subdir->cd();

	hist_collection.m_h_jetPt->Write();
	hist_collection.m_h_jetEta->Write();
	hist_collection.m_h_jetEtaPt->Write();
	hist_collection.m_h_jetEtaPhi->Write();

	hist_collection.m_h_jetJetFitter_mass->Write();
	hist_collection.m_h_jetJetFitter_energy_fraction->Write();
	hist_collection.m_h_jetJetFitter_significance3d->Write();
	hist_collection.m_h_jetJetFitter_nVTX->Write();
	hist_collection.m_h_jetJetFitter_nSingleTracks->Write();
	hist_collection.m_h_jetJetFitter_nTracksAtVtx->Write();
	hist_collection.m_h_jetJetFitter_N2Tpair->Write();
	hist_collection.m_h_jetJetFitter_deltaR->Write();

	hist_collection.m_h_jetIP2D_bu->Write();
	hist_collection.m_h_jetIP2D_bc->Write();
	hist_collection.m_h_jetIP2D_cu->Write();
	hist_collection.m_h_jetIP3D_bu->Write();
	hist_collection.m_h_jetIP3D_bc->Write();
	hist_collection.m_h_jetIP3D_cu->Write();

	hist_collection.m_h_jetDL1->Write();
	hist_collection.m_h_jetDL1_two_flav->Write();	
	hist_collection.m_h_jetDL1_pb->Write();
	hist_collection.m_h_jetDL1_pc->Write();
	hist_collection.m_h_jetDL1_pu->Write();

	hist_collection.m_h_jetSV1_NGTinSvx->Write();
	hist_collection.m_h_jetSV1_mass->Write();
	hist_collection.m_h_jetSV1_N2Tpair->Write();
	hist_collection.m_h_jetSV1_efracsvx->Write();
	hist_collection.m_h_jetSV1_deltaR->Write();
	hist_collection.m_h_jetSV1_Lxy->Write();
	hist_collection.m_h_jetSV1_L3d->Write();
	hist_collection.m_h_jetSV1_normdist->Write();

  hist_collection.m_h_jet_muon_dR->Write();
	hist_collection.m_h_jet_muon_pT->Write();
	hist_collection.m_h_jet_muon_pTRel->Write();

	hist_collection.m_h_numOfJets->Write(); 
	hist_collection.m_h_FCalEt_events_with_jet->Write(); 

}

double Btag::getWeight(double value, TH1D *weights)
{
	double wei = 0.;
	if(weights)
	{
		wei = weights->GetBinContent(weights->FindBin(value));
	}

	return wei;
}

double Btag::getWeight2D(double x_value, double y_value,  TH2D *weights2d)
{
        double wei = 0.;
        if(weights2d)
        {
                wei = weights2d->GetBinContent(weights2d->FindBin(x_value, y_value));
        }

        return wei;
}


