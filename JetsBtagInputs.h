#ifndef MyJets_JetsBtagInputs_H
#define MyJets_JetsBtagInputs_H

#include <vector>
#include <iostream>

#include<TFile.h>
#include<TString.h>
#include<TH1D.h>
#include<TH2F.h>

using namespace std;

class JetsBtagInputs
{
	public:
		// this is a standard algorithm constructor
		JetsBtagInputs (TString name);

		//virtual ~JetsBtagInputs();
		~JetsBtagInputs();

		TString m_subDir_name;


	    /// all jet histograms collections
		vector<TH1D *> m_Jet_hist;

		TH1D * m_h_numOfJets;
//		TH1D * m_h_numOfTruthJets;
		TH1D * m_h_FCalEt_events_with_jet; 

//		TH1D * m_h_numOfBJets;
//		TH1D * m_h_numOfTruthBJets;
//	    TH1D * m_h_truthJetsLabels;

		TH1D * m_h_jetPt;
		TH1D * m_h_jetEta;
        TH2D * m_h_jetEtaPt;
        TH2D * m_h_jetEtaPhi;
//		TH1D * m_h_badjetPt;
//	    TH1D * m_h_weirdjetPt;

		TH1D * m_h_jetJetFitter_mass;
		TH1D * m_h_jetJetFitter_energy_fraction;
		TH1D * m_h_jetJetFitter_significance3d;
		TH1D * m_h_jetJetFitter_nVTX;
		TH1D * m_h_jetJetFitter_nSingleTracks;
		TH1D * m_h_jetJetFitter_nTracksAtVtx;
		TH1D * m_h_jetJetFitter_N2Tpair;
		TH1D * m_h_jetJetFitter_deltaR;

		TH1D * m_h_jetIP2D_bu;
		TH1D * m_h_jetIP2D_bc;
		TH1D * m_h_jetIP2D_cu;
		TH1D * m_h_jetIP3D_bu;
		TH1D * m_h_jetIP3D_bc;
		TH1D * m_h_jetIP3D_cu;

		TH1D * m_h_jetDL1;
		TH1D * m_h_jetDL1_two_flav;		
		TH1D * m_h_jetDL1_pb;
		TH1D * m_h_jetDL1_pc;
		TH1D * m_h_jetDL1_pu;

		TH1D * m_h_jetSV1_NGTinSvx;
		TH1D * m_h_jetSV1_mass;
		TH1D * m_h_jetSV1_N2Tpair;
		TH1D * m_h_jetSV1_efracsvx;
		TH1D * m_h_jetSV1_deltaR;
		TH1D * m_h_jetSV1_Lxy;
		TH1D * m_h_jetSV1_L3d;
		TH1D * m_h_jetSV1_normdist; 

		TH1D * m_h_jet_muon_pTRel;
		TH1D * m_h_jet_muon_pT;
		TH1D * m_h_jet_muon_dR;
		



};

#endif

#ifdef JetsBtagInputs_cxx
JetsBtagInputs::~JetsBtagInputs()
{
	delete m_h_numOfJets; 
	delete m_h_FCalEt_events_with_jet; 
	delete m_h_jetPt; 
	delete m_h_jetEta;
	delete m_h_jetEtaPt;
	delete m_h_jetEtaPhi;
	delete m_h_jetJetFitter_mass;
	delete m_h_jetJetFitter_energy_fraction;
	delete m_h_jetJetFitter_significance3d;
	delete m_h_jetJetFitter_nVTX;
	delete m_h_jetJetFitter_nSingleTracks;
	delete m_h_jetJetFitter_nTracksAtVtx;
	delete m_h_jetJetFitter_N2Tpair;
	delete m_h_jetJetFitter_deltaR;
	delete m_h_jetIP2D_bu;
	delete m_h_jetIP2D_bc;
	delete m_h_jetIP2D_cu;
	delete m_h_jetIP3D_bu;
	delete m_h_jetIP3D_bc;
	delete m_h_jetIP3D_cu;
	delete m_h_jetDL1;
	delete m_h_jetDL1_two_flav;
	delete m_h_jetDL1_pb;
	delete m_h_jetDL1_pc;
	delete m_h_jetDL1_pu;
	delete m_h_jetSV1_NGTinSvx;
	delete m_h_jetSV1_mass;
	delete m_h_jetSV1_N2Tpair;
	delete m_h_jetSV1_efracsvx;
	delete m_h_jetSV1_deltaR;
	delete m_h_jetSV1_Lxy;
	delete m_h_jetSV1_L3d;
	delete m_h_jetSV1_normdist;
	delete m_h_jet_muon_pTRel;
	delete m_h_jet_muon_pT;
	delete m_h_jet_muon_dR;
}
#endif
