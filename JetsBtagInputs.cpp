#define JetsBtagInputs_cxx
#include "JetsBtagInputs.h"


JetsBtagInputs :: JetsBtagInputs (TString name )
{
	cout << "Entering constructor of JetsBtagInputs" << endl;

	m_subDir_name = name;

	m_h_numOfJets = new TH1D("h_numOfJets", "h_numOfJets", 30, 0, 30); 
	m_h_FCalEt_events_with_jet = new TH1D("h_FCalEt_events_with_jet", "h_FCalEt_events_with_jet", 900, -1.0, 8.0);
//	m_h_numOfTruthJets = new TH1D("h_numOfTruthJets", "h_numOfTruthJets", 30, 0, 30);
//	m_h_numOfBJets = new TH1D("h_numOfBJets", "h_numOfBJets", 10, 0, 10);
//	m_h_numOfTruthBJets = new TH1D("h_numOfTruthBJets", "h_numOfTruthBJets", 10, 0, 10);
//	m_h_truthJetsLabels = new TH1D("h_truthJetsLabels", "h_truthJetsLabels", 200, -100, 100);

	//m_h_jetPt = new TH1D ("h_jetPt", "h_jetPt", 100, 0, 1000); // jet pt [GeV]
	m_h_jetPt = new TH1D ("h_jetPt", "h_jetPt", 1300, 0, 1300); // jet pt [GeV]
	m_h_jetEta = new TH1D ("h_jetEta", "h_jetEta", 100, -5, 5); // jet eta
	m_h_jetEtaPt = new TH2D ("h_jetEtaPt", "h_jetEtaPt", 42, -2.1, 2.1,  1300, 0, 1300); // jet eta x pt 
	const int nbins_phi = 64;
        const double PI  = 4.*atan(1.);
	m_h_jetEtaPhi = new TH2D ("h_jetEtaPhi", "h_jetEtaPhi", 42, -2.1, 2.1, nbins_phi, -PI, PI); // jet eta x phi 
//	m_h_badjetPt = new TH1D("h_badjetPt", "h_badjetPt", 100, 0, 1000); // jet pt [GeV]
//	m_h_weirdjetPt = new TH1D("h_weirdjetPt", "h_weirdjetPt", 100, 0, 1000); // jet pt [GeV]

	//// ##### JetFitter
	m_h_jetJetFitter_mass = new TH1D ("h_jetJetFitter_mass", "h_jetJetFitter_mass", 122, -100, 12000);
	m_h_jetJetFitter_energy_fraction = new TH1D ("h_jetJetFitter_energy_fraction", "h_jetJetFitter_energy_fraction", 122, -0.01, 1.21);
	m_h_jetJetFitter_significance3d = new TH1D ("h_jetJetFitter_significance3d", "h_jetJetFitter_significance3d", 101, -1, 100);
	m_h_jetJetFitter_nVTX = new TH1D ("h_jetJetFitter_nVTX", "h_jetJetFitter_nVTX", 10, 0, 10);
	m_h_jetJetFitter_nSingleTracks = new TH1D ("h_jetJetFitter_nSingleTracks", "h_jetJetFitter_nSingleTracks", 20, 0, 20);
	m_h_jetJetFitter_nTracksAtVtx = new TH1D ("h_jetJetFitter_nTracksAtVtx", "h_jetJetFitter_nTracksAtVtx", 30, 0, 30);
	m_h_jetJetFitter_N2Tpair = new TH1D ("h_jetJetFitter_N2Tpair", "h_jetJetFitter_N2Tpair", 100, 0, 500);
	m_h_jetJetFitter_deltaR = new TH1D ("h_jetJetFitter_deltaR", "h_jetJetFitter_deltaR", 1000, 0, 20);

	//// ##### IP2D
	m_h_jetIP2D_bu = new TH1D ("h_jetIP2D_bu", "h_jetIP2D_bu", 200, -100, 100);
	m_h_jetIP2D_bc = new TH1D ("h_jetIP2D_bc", "h_jetIP2D_bc", 200, -100, 100);
	m_h_jetIP2D_cu = new TH1D ("h_jetIP2D_cu", "h_jetIP2D_cu", 200, -100, 100);

	//// ##### IP3D
	m_h_jetIP3D_bu = new TH1D ("h_jetIP3D_bu", "h_jetIP3D_bu", 200, -100, 100);
	m_h_jetIP3D_bc = new TH1D ("h_jetIP3D_bc", "h_jetIP3D_bc", 200, -100, 100);
	m_h_jetIP3D_cu = new TH1D ("h_jetIP3D_cu", "h_jetIP3D_cu", 200, -100, 100);


	//// ##### DL1
	m_h_jetDL1 = new TH1D ("h_jetDL1", "h_jetDL1", 2000, -40, 60);
        /// a bit crazy binning from adams 70-80% class 
        //m_h_jetDL1 = new TH1D ("h_jetDL1", "h_jetDL1", 200, -5.2087870, 16.363342);
        /// a bit crazy binning from adams 30-40% class 
        //m_h_jetDL1 = new TH1D ("h_jetDL1", "h_jetDL1", 200, -5.4808338, 38.767668);
	m_h_jetDL1_two_flav = new TH1D ("h_jetDL1_two_flav", "h_jetDL1_two_flav", 200, -40, 60);
	m_h_jetDL1_pb = new TH1D ("h_jetDL1_pb", "h_jetDL1_pb", 110, 0, 1.1);
	m_h_jetDL1_pc = new TH1D ("h_jetDL1_pc", "h_jetDL1_pc", 110, 0, 1.1);
	m_h_jetDL1_pu = new TH1D ("h_jetDL1_pu", "h_jetDL1_pu", 110, 0, 1.1);

	//// ##### SV1
	m_h_jetSV1_NGTinSvx = new TH1D ("h_jetSV1_NGTinSvx", "h_jetSV1_NGTinSvx", 31, -1, 30);
	m_h_jetSV1_mass = new TH1D ("h_jetSV1_mass", "h_jetSV1_mass", 101, -100, 10000); // jet SV1_mass
	m_h_jetSV1_N2Tpair = new TH1D ("h_jetSV1_N2Tpair", "h_jetSV1_N2Tpair", 101, -1, 100);
	m_h_jetSV1_efracsvx = new TH1D ("h_jetSV1_efracsvx", "h_jetSV1_efracsvx", 202, -1.01, 1.01);
	m_h_jetSV1_deltaR = new TH1D ("h_jetSV1_deltaR", "h_jetSV1_deltaR", 100, 0, 10);
	m_h_jetSV1_Lxy = new TH1D ("h_jetSV1_Lxy", "h_jetSV1_Lxy", 600, -100, 500);
	m_h_jetSV1_L3d = new TH1D ("h_jetSV1_L3d", "h_jetSV1_L3d", 600, -100, 500);
	m_h_jetSV1_normdist = new TH1D ("h_jetSV1_normdist", "h_jetSV1_normdist", 101, -5, 500); // 3D vertex significance 

	///// histograms for pT rel templates
	m_h_jet_muon_pTRel = new TH1D ("h_jet_muon_pTRel", "h_jet_muon_pTRel", 100, 0, 5);
	m_h_jet_muon_pT = new TH1D ("h_jet_muon_pT", "h_jet_muon_pT", 100, 0, 1000);
	m_h_jet_muon_dR = new TH1D ("h_jet_muon_dR", "h_jet_muon_dR", 40, 0, 0.4);


}
